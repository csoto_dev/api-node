'use strict';

module.exports = (server) => {
  const db = server.dataSources.api_datasource;

  const automigrate = (ds) => {
    ds.isActual((err, actual) => {
      if (err) throw err;
      if (!actual) {
        ds.autoupdate((err) => {
          if (err) throw err;
          console.log('\nAutomigrate completed');
        });
      }
    });
    ds.migrated = true;
  };

  const checkConnection = () => {

    if (db.connected && !db.migrated) {
      automigrate(db);
    }
  };

  checkConnection();
  db.once('connected', checkConnection);
};
