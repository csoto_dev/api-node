'use strict';

const path = require('path');
const loopback = require('loopback');
const instadate = require('instadate');


module.exports = function(Person) {

  Person.beforeRemote('create', validateRegister);
  Person.afterRemote('create', processNewPerson);
  Person.afterRemote('find', getPersonData);
  Person.on('attached', onAttach);

  function onAttach() {
    Person.app.on('started', () => {
      Person.nestRemoting('profile');
    });
  }


  function validateRegister(ctx, person,next){
    if(!ctx.req.body.firstName || !ctx.req.body.lastName || !ctx.req.body.phone || !ctx.req.body.birthday || !ctx.req.body.country){
      return next('MISSING FIRST NAME, LAST NAME, PHONE, BIRTHDAY OR COUNTRY FOR PROFILE CREATION');
    }else{
      return next();
    }
  }
  async function processNewPerson(ctx, person, next) {
    try {
      const personId = person.id;

      const {Profile} = Person.app.models;

      await Promise.all([
        Profile.create({
          personId: personId,
          firstName: ctx.req.body.firstName,
          lastName: ctx.req.body.lastName,
          phone: ctx.req.body.phone,
          birthday: instadate.isoDateString(ctx.req.body.birthday),
          country: ctx.req.body.country
        })
      ]);


      return ctx;
    } catch (err) {
      return err;
    }
  }

  async function getPersonData(ctx, person, next) {
    try {
      const {profile} = Person.app.models;
      const resultData = ctx.result;
      ctx.result = [];
      let resultFormat={
        email: '',
        username: '',
        id: 0,
        firstName: '',
        lastName: '',
        phone: '',
        birthday: '',
        country: ''
      };

      for(let result of resultData){
        let profileData = await profile.findOne({where:{personId:result.id}});

        resultFormat.email=result.email;
        resultFormat.username=result.username;
        resultFormat.id=result.id;
        resultFormat.firstName=profileData.firstName;
        resultFormat.lastName=profileData.lastName;
        resultFormat.phone=profileData.phone
        resultFormat.birthday=profileData.birthday;
        resultFormat.country=profileData.country;

        ctx.result.push(resultFormat);
      }




      return ctx;
    } catch (err) {
      return err;
    }
  }



};
